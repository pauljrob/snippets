#!/usr/bin/env python3
import pdb
import json, csv, sys, pprint
from subprocess import call
from apiclient import maas_client

# How To
# 1.  Login on CLI first:  maas login admin http://localhost/MAAS/api/2.0
# 2.  Create a file with the system_id and fqdn separted by a space i.e 4y3hf7 sdp-isopsn-126.quickplay.local
# 2a.  Created a file because this script doesn't have logic to pick a range of hosts to run on (feature request)
#       and this can be easily done via a file
#
# You can create the nodes file like this via a CLI re-direct (>)
# all_nodes = client.get("nodes/").read()
# ndecoded=all_nodes.decode("utf-8")
# data = json.loads(ndecoded)
# for nodes in data:
#    print(nodes["system_id"] + " " + nodes["fqdn"])

APIKEY= 'cJGRWgcLyQw4eAjEr9:x95wSWp3m5kAqGVjVA:MG9UK6zMeUA5tzMDNCgJWyH8TdgkR4kK'
MAAS_URL= 'http://localhost/MAAS/api/2.0'

auth = maas_client.MAASOAuth(*APIKEY.split(":"))
client = maas_client.MAASClient(auth, maas_client.MAASDispatcher(), MAAS_URL)

file = "all-ready-nodes"
with open(file) as data_file:
        for nodes in data_file:
                node = nodes.split()
                #print(node[0])
                host_id = node[0]
                print(host_id + node[1])

                # Get Volume Group Info
                machineinfo = client.get("nodes/%s/volume-groups/" % (host_id)).read()
                mdecoded=machineinfo.decode("utf-8")
                mjsondata=json.loads(mdecoded)
                volume_group = mjsondata[0]["id"] # 146
                logical_volume = mjsondata[0]["logical_volumes"][0]["id"]

                # Get block device info
                machineinfo = client.get("nodes/%s/blockdevices/" % (host_id)).read()
                mdecoded = machineinfo.decode("utf-8")
                mjsondata = json.loads(mdecoded)
                #print(json.dumps(mjsondata, sort_keys=True, indent=4))

                sda = mjsondata[0]["id"] #290
                sdb = mjsondata[1]["id"] #291

                #Uncomment below for partition deletion
                machineinfo = client.get("nodes/%s/blockdevices/%s/partitions/" % (host_id, sda)).read()
                mdecoded=machineinfo.decode("utf-8")
                mjsondata=json.loads(mdecoded)
                partition = mjsondata[0]["id"]
                call('maas admin volume-group delete-logical-volume %s %s id=%s' % (host_id, volume_group, logical_volume), shell=True)
                call('maas admin volume-group delete %s %s' % (host_id, volume_group), shell=True)
                call('maas admin partition delete %s %s %s' % (host_id, sda, partition), shell=True)

                #Create RAID
                call('maas admin raids create %s name=md0 block_devices=%s block_devices=%s level=raid-1' % (host_id, sda, sdb), shell=True)

                machineinfo = client.get("nodes/%s/blockdevices/" % (host_id)).read()
                mdecoded = machineinfo.decode("utf-8")
                mjsondata = json.loads(mdecoded)
                raid_block_device_id = mjsondata[2]["id"]

                # Create FS and mountpoint
                call('maas admin block-device format %s %s fstype=ext4' % (host_id, raid_block_device_id), shell=True)
                call('maas admin block-device mount %s %s mount_point=/' % (host_id, raid_block_device_id), shell=True)
