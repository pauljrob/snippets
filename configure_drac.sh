#!/bin/bash
# On a Mac do the following:
# brew create https://sourceforge.net/projects/sshpass/files/sshpass/1.05/sshpass-1.05.tar.gz --force
# brew install sshpass

echo -n "Enter iDRAC root password (password will not be displayed): "
read -s DRACPASS
echo

echo -n "Enter chassis IP: "
read CHASSISIP
echo

echo -n "Enter server 1 IP: "
read SERVER1IP
echo

echo -n "Enter server 2 IP: "
read SERVER2IP
echo

echo -n "Enter server 3 IP: "
read SERVER3IP
echo

echo -n "Enter server 4 IP: "
read SERVER4IP
echo

echo -n "Do the previous values look correct? (yes/no): "
read STATUS
if [[ $STATUS == "yes" ]]; then
  echo "Applying configs..."
  echo
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 config -m server-1 -g cfgLanNetworking -o cfgNicIPv4Enable 1
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 config -g cfgLanNetworking -o cfgDNSServer1 10.177.62.32
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 config -g cfgLanNetworking -o cfgDNSServer2 10.177.62.33
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 setniccfg -m server-1 -s $SERVER1IP 255.255.252.0 10.25.8.1
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 setniccfg -m server-2 -s $SERVER2IP 255.255.252.0 10.25.8.1
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 setniccfg -m server-3 -s $SERVER3IP 255.255.252.0 10.25.8.1
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 setniccfg -m server-4 -s $SERVER4IP 255.255.252.0 10.25.8.1
  sshpass -p "$DRACPASS" ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.0.120 setniccfg -m chassis -s $CHASSISIP 255.255.252.0 10.25.8.1
else
  exit 1
fi
