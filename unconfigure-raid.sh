#!/bin/bash

for i in `cat rack1-nodes | grep -v "#"`
do
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm raid deletevd:Disk.Virtual.0:RAID.Integrated.1-1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm raid deletevd:Disk.Virtual.1:RAID.Integrated.1-1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm raid converttononraid:Disk.Bay.0:Enclosure.Internal.0-1:RAID.Integrated.1-1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm raid converttononraid:Disk.Bay.1:Enclosure.Internal.0-1:RAID.Integrated.1-1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm jobqueue create RAID.Integrated.1-1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i racadm serveraction powercycle
done

# racadm raid deletevd:Disk.Virtual.0:RAID.Integrated.1-1
# racadm raid deletevd:Disk.Virtual.1:RAID.Integrated.1-1
# racadm raid converttononraid:Disk.Bay.0:Enclosure.Internal.0-1:RAID.Integrated.1-1
# racadm raid converttononraid:Disk.Bay.1:Enclosure.Internal.0-1:RAID.Integrated.1-1
# racadm jobqueue create RAID.Integrated.1-1
# racadm serveraction powercycle
