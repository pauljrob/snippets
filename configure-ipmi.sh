#!/bin/bash

for i in `cat hosts-chassis | grep -v "#"`
do
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i config -g CfgServerInfo -o cfgServerIPMIOverLanEnable 1 -i 1
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i config -g CfgServerInfo -o cfgServerIPMIOverLanEnable 1 -i 2
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i config -g CfgServerInfo -o cfgServerIPMIOverLanEnable 1 -i 3
  sshpass -p calvin ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$i config -g CfgServerInfo -o cfgServerIPMIOverLanEnable 1 -i 4
done
