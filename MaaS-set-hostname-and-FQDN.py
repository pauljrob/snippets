#!/usr/bin/env python3
import pdb
import json, bson, csv, sys
APIKEY= 'cJGRWgcLyQw4eAjEr9:x95wSWp3m5kAqGVjVA:MG9UK6zMeUA5tzMDNCgJWyH8TdgkR4kK'
MAAS_URL= 'http://localhost/MAAS/api/2.0'

from apiclient import maas_client
auth = maas_client.MAASOAuth(*APIKEY.split(":"))
client = maas_client.MAASClient(auth, maas_client.MAASDispatcher(), MAAS_URL)

#nodeinfo = client.get("nodes/").read()
#ndecoded=nodeinfo.decode("utf-8")
#njsondata=json.loads(ndecoded)
#for entry in njsondata:
#  print(entry['fqdn'])

# Assume input file formatted with hostname and IP on each line, tab delimeted:
# hostname1	IP1
# hostname2	IP2
# etc
mappings={}
with open('hostname_to_ipmi_mapping_full.txt', 'r') as f:
  reader=csv.reader(f,delimiter='\t')
  for hostname,ip in reader:
    mappings[ip]=hostname

#pdb.set_trace()
#testo=json.loads(client.get("machines/4y3h7p/", "power_parameters").read().decode("utf-8"))['power_address']
machineinfo = client.get("machines/").read()
mdecoded=machineinfo.decode("utf-8")
mjsondata=json.loads(mdecoded)
for entry in mjsondata:
  #print(entry['power_type'])
  endpoint="machines/" + entry['system_id'] + "/"
  hostname=entry['hostname']
  power_address=json.loads(client.get(endpoint, "power_parameters").read().decode("utf-8"))['power_address']
  #print(power_address)
  if not mappings.get(power_address):
    print("Error: IPMI IP address %s registered to server not found in input file" % (power_address))
    continue
  else:
    # Set FQDN
    if entry['domain']['name'] != 'quickplay.local':
      print("Updating Domain name to quickplay.local for %s" % (hostname))
      client.put(endpoint, domain='1') # this is the domain index. domain 0 is default maas, and domain 1 in our case is quickplay.local

    # Set Hostname
    if hostname == mappings[power_address]:
      print("Hostname %s matches expected hostname for IPMI IP %s" % (hostname,power_address))
    else:
      print("Hostname %s does not match expected hostname for IPMI IP %s, setting Hostname to %s" % (hostname,power_address,mappings[power_address]))
      # Uncomment this line when we want to set hostnames
      client.put(endpoint, hostname=mappings[power_address])
